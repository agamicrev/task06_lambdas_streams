package part1;

/**
 * Create functional interface with method that accepts three int values
 * and return int value. Create lambda functions (as variables in main
 * method) what implements this interface:
 * First lambda returns max value
 * Second – average
 * Invoke thous lambdas.
 *
 */
public class App {

  public static void main(String[] args) {
    MyInterface firstLambda = (a, b, c) -> Math.max(a, Math.max(b, c));
    System.out.println("Max value (1, 2, 3): " + firstLambda.method(1, 2, 3));

    MyInterface secondLambda = (a, b, c) -> (a + b + c) / 3;
    System.out.println("Average (1, 2, 3): " + secondLambda.method(1, 2, 3));
  }

}

@FunctionalInterface
interface MyInterface {
  int method(int a, int b, int c);
}
