package part2;

public class Action implements Command{
  protected String name;
  Command command;
  Controller controller;

  public Action() {
  }

  public Action(String name){
    controller = new Controller();
    if(name.equals("Right")) command = new TurnRightCommand(controller);
    if(name.equals("Left")) command = new TurnLeftCommand(controller);
    if(name.equals("Up")) command = new TurnUpCommand(controller);
    if(name.equals("Down")) command = new TurnDownCommand(controller);

  }

  @Override
  public void execute(String name) {

  }
}
