package part2;

@FunctionalInterface
public interface Command {
  void execute(String name);
}

enum State {RIGHT, LEFT, UP, DOWN}