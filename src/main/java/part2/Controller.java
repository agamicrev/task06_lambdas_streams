package part2;

public class Controller {
  private State state;

  public Controller() {

  }

  public Controller(String name){
    if(name.equals("Right")) turnRight();
    if(name.equals("Left")) turnLeft();
    if(name.equals("Down")) turnDown();
    if(name.equals("Up")) turnUp();
  }

  public void turnLeft() { this.state = State.LEFT;
    System.out.println("Turning " + state.toString().toLowerCase()); }
  public void turnRight() { this.state = State.RIGHT;
    System.out.println("Turning " + state.toString().toLowerCase()); }
  public void turnUp() { this.state = State.UP;
    System.out.println("Turning " + state.toString().toLowerCase()); }
  public void turnDown() { this.state = State.DOWN;
    System.out.println("Turning " + state.toString().toLowerCase()); }
}
