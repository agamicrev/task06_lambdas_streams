package part2;

public class Invoker {
  private Command command;

  public void setCommand(Command command){
    String name = "";
    this.command = command;
    command.execute(name);
  }

  public void setCommand(Command command, String name){
    this.command = command;
    command.execute(name);
  }
}
