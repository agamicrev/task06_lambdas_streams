package part2;

public class TurnDownCommand extends Action implements Command {
  Controller controller;
  public TurnDownCommand(Controller controller){
    this.controller = controller;
  }

  public void execute(String name){
    controller.turnDown();
  }
}
