package part2;

public class TurnLeftCommand extends Action implements Command{
  Controller controller;
  public TurnLeftCommand(Controller controller){
    this.controller = controller;
  }

  public void execute(String name){
    controller.turnLeft();
  }
}
