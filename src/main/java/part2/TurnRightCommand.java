package part2;

public class TurnRightCommand extends Action implements Command {
  Controller controller;
  public TurnRightCommand(Controller controller){
    this.controller = controller;
  }

  public void execute(String name){
    controller.turnRight();
  }
}
