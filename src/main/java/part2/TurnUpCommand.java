package part2;

public class TurnUpCommand extends Action implements Command {
  Controller controller;
  public TurnUpCommand(Controller controller){
    this.controller = controller;
  }

  public void execute(String name){
    controller.turnUp();
  }
}
