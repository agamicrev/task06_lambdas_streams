package part2;

import java.util.Scanner;

public class User {
  public static void main(String[] args) {

    String name;
    Scanner in = new Scanner(System.in);

    Invoker invoker = new Invoker();

    System.out.print("Input name: ");
    name = in.next();
    String finalName = name;

    System.out.println("Through lambda expression");
    invoker.setCommand((i) -> new Controller(finalName));

    System.out.println("Through anonymous class");
    invoker.setCommand(new Command() { public void execute(String name) {new Controller(finalName);}});

    System.out.println("Through method reference");
    invoker.setCommand(Controller::new, finalName);

    System.out.println("Through object");
    Action action = new Action(finalName);
    invoker.setCommand(action.command);
  }
}
