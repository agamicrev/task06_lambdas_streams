package part3;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Create a few methods that returns list (or array) of random integers. Methods should use streams
 * API and should be implemented using different Streams generators. Count average, min, max, sum of
 * list values. Try to count sum using both reduce and sum Stream methods. Count number of values
 * that are bigger than average
 */
public class App {

  public static void main(String[] args) {

    Integer max, min, sum;
    double average;
    long amountOfGreaterThanAverage;

    List<Integer> list;
    list = random();
    System.out.println("List of random integers:");
    list.forEach(i -> System.out.print(i + " "));

    average = list.stream().mapToInt(Integer::intValue).average().getAsDouble();
    System.out.println("\n\nAverage: " + average);

    min = list.stream().reduce(Integer::min).get();
    System.out.println("\nMinimum (method reduce): " + min);

    max = list.stream().max(Integer::compareTo).get();
    System.out.println("\nMaximum (method max): " + max);

    sum = list.stream().reduce(0, Integer::sum);
    System.out.println("\nSum (method reduce): " + sum);

    sum = list.stream().mapToInt(Integer::intValue).sum();
    System.out.println("\nSum (method mapToInt): " + sum);

    amountOfGreaterThanAverage = list.stream().filter(i -> (i > average)).count();
    System.out.println("\nBigger than average: " + amountOfGreaterThanAverage);

  }

  public static List<Integer> random() {

    List<Integer> myList = IntStream
        .generate(() -> ThreadLocalRandom.current()
            .nextInt(40))
        .limit(10)
        .boxed().collect(Collectors.toList());

    return myList;
  }

}
