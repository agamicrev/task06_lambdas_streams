package part4;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * Create application. User enters some number of text lines (stop reading text when user enters
 * empty line). Application returns:
 * - Number of unique words
 * - Sorted list of all unique words
 * - Word count. Occurrence number of each word in the text
 *   (e.g. text “a s a” -> a-2 s-1 ). Use grouping collector.
 * - Occurrence number of each symbol except upper case characters.
 *
 */

public class App {

  List<String> list = new ArrayList<>();

  public void run() {
    try (Scanner scanner = new Scanner(System.in)) {
      while (true) {
        String input = scanner.nextLine();
        if (input.isEmpty()) {
          break;
        }
        list.add(input);
      }
    }

    printStats();
  }

  private void printStats() {
    System.out.println("Number of unique words:");
    System.out.println(list.stream()
        .distinct()
        .count());
    System.out.println("\nSorted list of all unique words:");
    System.out.println(list.stream()
        .distinct()
        .sorted()
        .collect(Collectors.toList()));
    System.out.println("\nOccurrence number of each word in the text:");
    System.out.println(list.stream()
        .collect(Collectors.groupingBy(i -> i, Collectors.counting())));
    System.out.println("\nOccurrence number of each symbol except upper case characters:");
    System.out.println(list.stream()
        .flatMap(i -> i.chars().boxed())
        .map(i -> (char) i.intValue())
        .collect(Collectors.groupingBy(i -> i, Collectors.counting())));
  }

  public static void main(String[] args) {
    System.out.println("Please, input some words...");
    new App().run();
  }
}
